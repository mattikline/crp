<?php require "header.php"; 
$id = $_SESSION['userId'];
echo "$id" ?>

<main>
<h1>Edit Profile</h1>
<form action="includes/edit_profile.inc.php" method="post">
    <input type="text" name="Fname" placeholder="First name"><br>
    <input type="text" name="Minit" placeholder="Middle initial"><br>
    <input type="text" name="Lname" placeholder="Last name"><br>
    <input type="text" name="Bdate" placeholder="YYYY-MM-DD"><br>
    <input type = "hidden" name = "id" value = <?php echo "$id" ?>>
    <select name="Major">
        <option value="none"></option>
        <option value="COSC">COSC</option>
    </select><br>
    <button type="submit" name="profile-submit">Submit changes</button>
</form>
</main>

<?php require "footer.php"; ?>