<?php

$servername ="localhost";
$dBUsername = "root";
$dBPassword ="";
$dBName = "main_database";

// Signs into database with above values, local database must have the name "main_database"
$conn = mysqli_connect($servername, $dBUsername, $dBPassword, $dBName);

if (!$conn) {
    die("Connection failed: ".mysqli_connect_error());
}