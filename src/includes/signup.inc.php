<?php

// Checks if user arrived to this page by clicking the signup submit button
if (isset($_POST['signup-submit'])) {
    require 'dbh.inc.php';

    // Declares variables and gives them the values submitted from the signup.php page
    $username = $_POST['username'];
    $studentID = $_POST['studentID'];
    $password = $_POST['pwd'];
    $passwordRepeat = $_POST['pwd-repeat'];

    // Error checking for if any fields are empty
    if (empty($username) || empty($studentID) || empty($password) || empty($passwordRepeat)) {
        header("Location: ../signup.php?error=emptyfields&username=".$username."&studentID=".$studentID);
        exit();
    }

    // Error checking for if invalid characters are used in both the username and student ID (letters and numbers on in the username and numbers only in the ID)
    else if(!preg_match("/^[0-9]*$/", $studentID) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location: ../signup.php?error=invalideuserID");
        exit();
    }

    // Error checking for if invalid characters used in student ID. username field still stays filled when page returns to profile.php
    else if (!preg_match("/^[0-9]*$/", $studentID)) {
        header("Location: ../signup.php?error=invalidstudentID&username=".$username);
        exit();
    }

    // Error checking for if invalid characters used in username. studnent ID field still stay filled
    else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location: ../signup.php?error=invalidusername&studentID=".$studentID);
        exit();
    }

    // Error checking for if the password repeat doesn't match. Keeps the username and student ID fields filled
    else if ($password !== $passwordRepeat) {
        header("Location: ../signup.php?error=passwordcheck&username=".$username."&studentID=".$studentID);
        exit();
    }
    else {

        // String for searching database
        $sql = "SELECT Username FROM student WHERE Username=?";
        $stmt = mysqli_stmt_init($conn);

        // Checks for sql error
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../signup.php?error=sqlerror1");
            exit();
        }
        else {
            $idCheck = "Select Student_ID FROM student WHERE Student_ID = '$studentID'";
            $result = mysqli_query($conn, $idCheck);
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);

            // Error checking for if username has been taken
            if ($resultCheck > 0) {
                header("Location: ../signup.php?error=usertaken&studentID=".$studentID);
                exit();
            }
            if (mysqli_num_rows($result) > 0){
                    // Hashes password
                    $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
                    $sql = "UPDATE student set Username = '$username', Pass = '$hashedPwd' WHERE Student_ID = '$studentID'";
                    mysqli_query($conn, $sql);
                    header("Location: ../signup.php?signup=success");
                    exit();
                
            } 
            else {
                $sql = "INSERT INTO student (Username, Student_ID, Pass) VALUES (?, ?, ?)";
                $stmt = mysqli_stmt_init($conn);

                // Checks for sql error
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location: ../signup.php?error=sqlerror2");
                    exit();
                }
                else {
                    // Hashes password
                    $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($stmt, "sss", $username, $studentID, $hashedPwd);
                    mysqli_stmt_execute($stmt);
                    header("Location: ../signup.php?signup=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
}

// Redirects user to signup.php if they tried to access the page from someplace else than the signup submit button
else {
    header("Location: ../signup.php");
    exit();
} 